﻿namespace EjemploWCF.Host
{
	#region Usings

	using System;
	using System.ServiceModel;
	using System.ServiceModel.Description;
	using EjemploWCF.Fachada;
	using EjemploWCF.Interfaces;

	#endregion

	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Iniciando wcf");

			try {			
				var baseAddress = string.Format ("http://{0}:8000/FachadaServicio", Environment.MachineName);

				using (var host = new ServiceHost (typeof(FachadaServicio), new Uri (baseAddress))) {

					host.AddServiceEndpoint (typeof(IContratoServicio), new WebHttpBinding (), "")
						.Behaviors.Add (new WebHttpBehavior ());

					host.Open ();

					Console.WriteLine ("Servicio iniciado");

					Console.WriteLine ("Presione cualquier tecla para terminar el servicio...");

					Console.ReadLine ();

				}

			} catch (Exception ex) {
				Console.WriteLine (ex.Message);
			}
		}
	}
}
