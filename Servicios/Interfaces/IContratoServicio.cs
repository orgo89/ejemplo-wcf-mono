﻿namespace EjemploWCF.Interfaces
{
	#region Usings

	using System.ServiceModel;
	using System.ServiceModel.Web;

	#endregion

	/// <summary>
	/// I contrato servicio.
	/// </summary>
	[ServiceContract]
	public interface IContratoServicio
	{
		#region Métodos

		[OperationContract]
		[WebGet]
		string ObtenerMensaje ();

		#endregion
	}
}

