﻿namespace EjemploWCF.Fachada
{
	#region Usings

	using System;
	using Interfaces;

	#endregion
	/// <summary>
	/// Fachada servicio.
	/// </summary>
	public class FachadaServicio : IContratoServicio
	{
		#region Constructores

		/// <summary>
		/// Initializes a new instance of the <see cref="Fachada.FachadaServicio"/> class.
		/// </summary>
		public FachadaServicio ()
		{
		}

		#endregion

		#region Métodos Públicos

		#region IContratoServicio implementation

		public string ObtenerMensaje ()
		{
			return "Hola ke ase?";
		}

		#endregion

		#endregion
	}
}

